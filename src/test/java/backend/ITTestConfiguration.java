package backend;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(
        basePackages = {
                "backend.excuse"
        }
)
public class ITTestConfiguration {
}
