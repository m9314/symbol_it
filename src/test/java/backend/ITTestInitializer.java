package backend;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;

public class ITTestInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        PostgreSQLContainer sqlContainer = new PostgreSQLContainer("postgres:14")
                .withDatabaseName("symbol_it")
                .withUsername("admin")
                .withPassword("admin");
        sqlContainer.start();

        TestPropertyValues.of(
                "symbolit.database.hostname=" + sqlContainer.getContainerIpAddress(),
                "symbolit.database.port=" + sqlContainer.getFirstMappedPort(),
                "symbolit.database.username=" + sqlContainer.getUsername(),
                "symbolit.database.password=" + sqlContainer.getPassword()
        ).applyTo(configurableApplicationContext.getEnvironment());
    }
}
