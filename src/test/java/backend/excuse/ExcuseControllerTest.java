package backend.excuse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class ExcuseControllerTest {

    @Mock
    private ExcuseService excuseService;

    private ExcuseController excuseController;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        excuseController = new ExcuseController(excuseService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(excuseController).build();
    }

    @DisplayName("Successfully create an excuse")
    @Test
    void createExcuse() throws Exception {
        when(excuseService.createExcuse(eq(700), eq(ExcuseTag.FUCKING), eq("Fucking Test")))
                .thenReturn(new ExcuseJPA(1, 700, ExcuseTag.FUCKING, "Fucking Test"));
        mockMvc.perform(post("/excuses")
                        .contentType(APPLICATION_JSON)
                        .content("{\"http_code\": 700, \"tag\": \"Fucking\", \"message\": \"Fucking Test\"}"))
                .andExpect(status().is(HttpStatus.CREATED.value()));

        verify(excuseService, times(1)).createExcuse(eq(700), eq(ExcuseTag.FUCKING), eq("Fucking Test"));
    }

    @DisplayName("Failed to create an excuse with an unknown tag")
    @Test
    void createExcuseWithUnknownTag() throws Exception {
        mockMvc.perform(post("/excuses")
                        .contentType(APPLICATION_JSON)
                        .content("{\"http_code\": 700, \"tag\": \"Fuking\", \"message\": \"Fucking Test\"}"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().string("Unknown tag Fuking. Must be one of : INEXCUSABLE,NOVELTY_IMPLEMENTATIONS,EDGE_CASES,FUCKING,SYNTAX_ERRORS,SUBSTANCE,PREDICTABLE_PROBLEMS,SOMEBODY_ELSE_PROBLEM,INTERNET_CRASHED"));

        verify(excuseService, never()).createExcuse(anyInt(), any(), anyString());
    }

    @DisplayName("Failed to create an excuse without any tag")
    @Test
    void createExcuseWithoutTag() throws Exception {
        mockMvc.perform(post("/excuses")
                        .contentType(APPLICATION_JSON)
                        .content("{\"http_code\": 700, \"message\": \"Fucking Test\"}"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));

        verify(excuseService, never()).createExcuse(anyInt(), any(), anyString());
    }

    @DisplayName("Failed to create an excuse with an empty tag")
    @Test
    void createExcuseWithEmptyTag() throws Exception {
        mockMvc.perform(post("/excuses")
                        .contentType(APPLICATION_JSON)
                        .content("{\"http_code\": 700, \"tag\": , \"message\": \"Fucking Test\"}"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));

        verify(excuseService, never()).createExcuse(anyInt(), any(), anyString());
    }

    @DisplayName("Failed to create an excuse without any message")
    @Test
    void createExcuseWithoutMessage() throws Exception {
        mockMvc.perform(post("/excuses")
                        .contentType(APPLICATION_JSON)
                        .content("{\"http_code\": 700, \"tag\": \"Fucking\"}"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));

        verify(excuseService, never()).createExcuse(anyInt(), any(), anyString());
    }

    @DisplayName("Failed to create an excuse with an empty message")
    @Test
    void createExcuseWithEmptyMessage() throws Exception {
        mockMvc.perform(post("/excuses")
                        .contentType(APPLICATION_JSON)
                        .content("{\"http_code\": 700, \"tag\": \"Fucking\", \"message\":}"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));

        verify(excuseService, never()).createExcuse(anyInt(), any(), anyString());
    }

    @DisplayName("Get all excuses")
    @Test
    void getAllExcuses() throws Exception {
        when(excuseService.getAllExcuses())
                .thenReturn(Collections.singletonList(new ExcuseJPA(1, 700, ExcuseTag.FUCKING, "Fucking Test")));
        mockMvc.perform(get("/excuses"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().json("[{\"http_code\": 700, \"tag\": \"Fucking\", \"message\":\"Fucking Test\"}]"));
    }

    @DisplayName("Get one excuse by its http code")
    @Test
    void getExcuse() throws Exception {
        when(excuseService.getExcuseByHttpCode(eq(700)))
                .thenReturn(new ExcuseJPA(1, 700, ExcuseTag.FUCKING, "Fucking Test"));
        mockMvc.perform(get("/excuses/http-code/700"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().json("{\"http_code\": 700, \"tag\": \"Fucking\", \"message\":\"Fucking Test\"}"));
    }

    @DisplayName("Get one excuse by its http code")
    @Test
    void getUnknownExcuse() throws Exception {
        when(excuseService.getExcuseByHttpCode(eq(700)))
                .thenReturn(null);
        mockMvc.perform(get("/excuses/http-code/700"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

}