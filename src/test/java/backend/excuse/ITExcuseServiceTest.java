package backend.excuse;

import backend.ITTestInitializer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ContextConfiguration(initializers = {ITTestInitializer.class})
class ITExcuseServiceTest {

    @Autowired
    private ExcuseService excuseService;

    @DisplayName("Verify that all initial excuses are in DB, create one and try and get it")
    @Test
    void createAndGetExcuses() {
        // database should be initialized with examples
        List<ExcuseJPA> excuses = excuseService.getAllExcuses();
        assertThat(excuses, hasSize(76));

        // create a new one
        ExcuseJPA newExcuse = excuseService.createExcuse(740, ExcuseTag.FUCKING, "test");
        assertEquals(740, newExcuse.getHttpCode());
        assertEquals(ExcuseTag.FUCKING, newExcuse.getTag());
        assertEquals("test", newExcuse.getMessage());

        // database should have one more
        excuses = excuseService.getAllExcuses();
        assertThat(excuses, hasSize(77));

        // database should have the one we created
        ExcuseJPA createdExcuse = excuseService.getExcuseByHttpCode(740);
        assertEquals(740, createdExcuse.getHttpCode());
        assertEquals(ExcuseTag.FUCKING, createdExcuse.getTag());
        assertEquals("test", createdExcuse.getMessage());
    }

    @DisplayName("Failed while trying to create an excuse with an existing http code")
    @Test
    void createExcuseWithExistingCode_shouldFail() {

        DataIntegrityViolationException ex = assertThrows(DataIntegrityViolationException.class, () ->
                excuseService.createExcuse(739, ExcuseTag.FUCKING, "test"));

        assertNotNull(ex.getMessage());
        assertTrue(ex.getMessage().contains("constraint [excuse_http_code_key]"));

    }
}