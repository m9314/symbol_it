package backend.excuse;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExcuseTagTest {

    @DisplayName("Assure that all ExcuseTag can be accessed by their designation")
    @Test
    void testGetExcuseByDesignation() {
        for (ExcuseTag tag : ExcuseTag.values()) {
            assertEquals(tag, ExcuseTag.getExcuseByDesignation(tag.getDesignation()));
        }
    }

}