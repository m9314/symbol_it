create table if not exists excuse
(
    id          bigserial       primary key,
    http_code   int8            not null unique,
    tag         varchar(50)     not null,
    message     varchar(250)    not null
    );