package backend;

import backend.excuse.ExcuseJPA;
import backend.excuse.ExcuseJSON;
import backend.excuse.ExcuseRepository;
import backend.excuse.ExcuseTag;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@Configuration
@ComponentScan(
        basePackages = {"backend.excuse"}
)
@EnableJpaRepositories(
        basePackages = {"backend.excuse"}
)
@EnableTransactionManagement()
@Slf4j
public class ApplicationConfiguration {

    public static final String JPA_VENDOR_ADAPTER = "jpaVendorAdapter";
    public static final String DATA_SOURCE = "dataSource";
    public static final String JPA_PROPERTIES = "jpaProperties";
    public static final String ENTITY_MANAGER_FACTORY = "entityManagerFactory";
    public static final String ENTITY_MANAGER = "entityManager";
    public static final String TRANSACTION_MANAGER = "transactionManager";
    public static final String FLYWAY = "flyway";


    @Value("${symbolit.database.driver-class}")
    private String hibernateDriverClass;

    @Value("${symbolit.database.type}")
    private String hibernateDatabase;

    @Value("${symbolit.database.hostname}")
    private String databaseHostname;

    @Value("${symbolit.database.port}")
    private int databasePort;

    @Value("${symbolit.database.username}")
    private String databaseUsername;

    @Value("${symbolit.database.password}")
    private String databasePassword;

    @Bean
    CommandLineRunner initDatabase(ExcuseRepository repository) {

        return args -> {
            if (repository.count() == 0) {
                log.info("Filling database with excuses' examples for the first time");
                ObjectMapper mapper = new ObjectMapper();
                repository.saveAll(
                        mapper.readValue(getClass().getClassLoader().getResourceAsStream("excuse_examples.json"), new TypeReference<List<ExcuseJSON>>() {
                                })
                                .stream()
                                .filter(excuseJSON -> ExcuseTag.getExcuseByDesignation(excuseJSON.getTag()) != null)
                                .map(excuseJSON ->
                                        ExcuseJPA.builder()
                                                .httpCode(excuseJSON.getHttpCode())
                                                .tag(ExcuseTag.getExcuseByDesignation(excuseJSON.getTag()))
                                                .message(excuseJSON.getMessage())
                                                .build()
                                )
                                .collect(Collectors.toList())
                );
                log.info("Database filled");
            }
        };
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean(JPA_VENDOR_ADAPTER)
    public HibernateJpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();

        jpaVendorAdapter.setShowSql(false);
        jpaVendorAdapter.setGenerateDdl(false);
        jpaVendorAdapter.setDatabase(Database.valueOf(hibernateDatabase));

        return jpaVendorAdapter;
    }

    @Bean(DATA_SOURCE)
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(hibernateDriverClass);
        dataSource.setJdbcUrl(String.format("jdbc:postgresql://%s:%s/symbol_it",
                databaseHostname,
                databasePort
        ));
        dataSource.setUsername(databaseUsername);
        dataSource.setPassword(databasePassword);
        dataSource.setPoolName(DATA_SOURCE);
        return dataSource;
    }

    @Bean(JPA_PROPERTIES)
    public Properties shippeoJpaProperties() {
        return new Properties();
    }

    @Bean(ENTITY_MANAGER_FACTORY)
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @Qualifier(JPA_VENDOR_ADAPTER) JpaVendorAdapter jpaVendorAdapter,
            @Qualifier(DATA_SOURCE) DataSource dataSource,
            @Qualifier(JPA_PROPERTIES) Properties jpaProperties
    ) {
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(dataSource);
        entityManagerFactory.setPersistenceUnitName(ENTITY_MANAGER);
        entityManagerFactory.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactory.setPackagesToScan(
                "backend.excuse"
        );
        entityManagerFactory.setJpaProperties(jpaProperties);
        return entityManagerFactory;
    }

    @Bean(name = TRANSACTION_MANAGER)
    @Primary
    public PlatformTransactionManager shippeoTransactionManager(
            @Qualifier(ENTITY_MANAGER_FACTORY) EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

    /**
     * Migration of database
     */
    @Bean(FLYWAY)
    public Flyway flyway(@Qualifier(DATA_SOURCE) DataSource dataSource) {
        Flyway flyway = Flyway.configure()
                .locations("classpath:db/migration")
                .dataSource(dataSource)
                .outOfOrder(true)
                .load();
        flyway.migrate();
        return flyway;
    }
}
