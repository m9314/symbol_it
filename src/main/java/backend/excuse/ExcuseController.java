package backend.excuse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/excuses")
public class ExcuseController {

    private final ExcuseService excuseService;

    @Autowired
    public ExcuseController(
            ExcuseService excuseService
    ){
        this.excuseService = excuseService;
    }

    @GetMapping
    public List<ExcuseJSON> getAllExcuses(){
        return excuseService.getAllExcuses()
                .stream()
                .map(ExcuseJSON::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/http-code/{httpCode}")
    public ResponseEntity<Object> getAllExcuses(@PathVariable("httpCode") int httpCode){
        ExcuseJPA excuse = excuseService.getExcuseByHttpCode(httpCode);
        if(excuse == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(new ExcuseJSON(excuse));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> createExcuse(@RequestBody @Validated(ExcuseJSON.Create.class) ExcuseJSON excuseJSON) throws URISyntaxException {
        ExcuseTag tag = ExcuseTag.getExcuseByDesignation(excuseJSON.getTag());
        if (tag == null) {
            return ResponseEntity.badRequest().body(String.format("Unknown tag %s. Must be one of : %s",
                    excuseJSON.getTag(),
                    Arrays.stream(ExcuseTag.values()).map(Enum::name).collect(Collectors.joining(","))
            ));
        }
        return ResponseEntity.created(new URI("")).body(new ExcuseJSON(
                excuseService.createExcuse(
                        excuseJSON.getHttpCode(),
                        tag,
                        excuseJSON.getMessage()
                ))
        );
    }
}
