package backend.excuse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExcuseService {

    private final ExcuseRepository excuseRepository;

    @Autowired
    public ExcuseService(
            ExcuseRepository excuseRepository
    ){
        this.excuseRepository = excuseRepository;
    }

    public List<ExcuseJPA> getAllExcuses(){
        return excuseRepository.findAll();
    }

    public ExcuseJPA getExcuseByHttpCode(int httpCode) {
        return excuseRepository.findByHttpCode(httpCode);
    }

    public ExcuseJPA createExcuse(int httpCode, ExcuseTag tag, String message) {
        return excuseRepository.save(
                ExcuseJPA.builder()
                        .httpCode(httpCode)
                        .tag(tag)
                        .message(message)
                        .build()
        );
    }
}
