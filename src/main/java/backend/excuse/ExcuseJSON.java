package backend.excuse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

@Getter
@NoArgsConstructor
@ToString
public class ExcuseJSON {

    @JsonProperty("http_code")
    private int httpCode;

    @Valid
    @NotNull(groups = Create.class)
    private String tag;

    @Valid
    @NotNull(groups = Create.class)
    private String message;

    /**
     * Deep copy of entity to JSON
     */
    public ExcuseJSON(ExcuseJPA excuseJPA) {
        this.httpCode = excuseJPA.getHttpCode();
        this.tag = excuseJPA.getTag().getDesignation();
        this.message = excuseJPA.getMessage();
    }

    public interface Create extends Default {
    }
}
