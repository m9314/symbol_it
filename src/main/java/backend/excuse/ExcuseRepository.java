package backend.excuse;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExcuseRepository extends JpaRepository<ExcuseJPA, Long> {
    ExcuseJPA findByHttpCode(int httpCode);
}
