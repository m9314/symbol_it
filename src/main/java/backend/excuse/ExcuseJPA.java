package backend.excuse;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "excuse")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ExcuseJPA {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "http_code", updatable = false, nullable = false)
    @NotNull
    private int httpCode;

    @Column(name = "tag", nullable = false)
    @NotNull
    private ExcuseTag tag;

    @Column(name = "message", nullable = false)
    @NotNull
    private String message;
}
