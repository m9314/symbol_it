package backend.excuse;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum ExcuseTag {
    INEXCUSABLE("Inexcusable"),
    NOVELTY_IMPLEMENTATIONS("Novelty Implementations"),
    EDGE_CASES("Edge Cases"),
    FUCKING("Fucking"),
    SYNTAX_ERRORS("Syntax Errors"),
    SUBSTANCE("Substance"),
    PREDICTABLE_PROBLEMS("Predictable Problems"),
    SOMEBODY_ELSE_PROBLEM("Somebody Else's Problem"),
    INTERNET_CRASHED("Internet crashed");

    private static Map<String, ExcuseTag> EXCUSE_TAG_BY_DESIGNATION;

    private final String designation;

    ExcuseTag(String designation){
        this.designation = designation;
    }

    public String getDesignation() {
        return designation;
    }

    static {
        EXCUSE_TAG_BY_DESIGNATION = Arrays.stream(ExcuseTag.values()).collect(Collectors.toMap(ExcuseTag::getDesignation, excuseTag -> excuseTag));
    }

    public static ExcuseTag getExcuseByDesignation(String designation){
        return EXCUSE_TAG_BY_DESIGNATION.get(designation);
    }
}
