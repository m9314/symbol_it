# Symbol IT test

This project is a technical test for Symbol IT

# How to start the app

You can start the app in a docker container. To do so, you can use those commands (while being in root directory)
in you terminal :

`mvn clean install -DskipTests`

`docker build -t app.jar .`

`docker-compose up -d `

Those commands will create 2 containers :

- excuses-de-dev_PostgreSQL_1
- excuses-de-dev_API_1

# How to use the app

Api container expose to routes at port 8080 :

- GET on /excuses : this will list all the excuses according to this schema :
  `[{"http_code": httpCode, "tag": "Tag", "message": "Message"}]`
- GET on /excuses/http-code/{code} : this will return the excuse of given code according to this schema :
  `{"http_code": httpCode, "tag": "Tag", "message": "Message"}`
- POST on /excuses : this will create a new excuse. This will need this schema as
  input : `{"http_code": httpCode, "tag": "Tag", "message": "Message"}`. All the fields are mandatory, HttpCode must be unique 
and Tag must be one on those options : INEXCUSABLE, NOVELTY_IMPLEMENTATIONS,
EDGE_CASES, FUCKING, SYNTAX_ERRORS, SUBSTANCE, PREDICTABLE_PROBLEMS, SOMEBODY_ELSE_PROBLEM, INTERNET_CRASHED.